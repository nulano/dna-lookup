/*
    DNA Lookup
    Copyright (C) 2016  Nulano (Ondrej Baranovic)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.nulano.dnalookup.swing;

import eu.nulano.dnalookup.data.DNA;

import javax.swing.*;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataListener;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Hashtable;

/**
 * Created by Nulano on 7. 6. 2016.
 *
 * @author Nulano
 */
public class MainFrame extends JFrame {

	private MainFrame() {
		setResizable(false);
		setTitle("DNA Lookup v1.0 by Nulano");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		JMenu about = new JMenu("About");
		setJMenuBar(new JMenuBar());
		getJMenuBar().add(about);
		about.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(MainFrame.this, "by Nulano", "About", JOptionPane.INFORMATION_MESSAGE);
			}
		});

		JSplitPane contentPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		{
			JTabbedPane left = new JTabbedPane();
			{
				JPanel dna = new JPanel(new BorderLayout());
				{
					JPanel dnaToolbar = new JPanel(new BorderLayout());
					{
						JPanel dnaView = new JPanel();
						{
							dnaView.add(new JLabel("View: "));

							JComboBox dnaCombo = new JComboBox(DNA.Mode.values());
							dnaCombo.setSelectedItem(DNA.Mode.mRNA);
							dnaView.add(dnaCombo);
						}
						dnaToolbar.add(dnaView, BorderLayout.WEST);

						JPanel dnaFilter = new JPanel(new BorderLayout());
						{
							dnaFilter.add(new JLabel("Filter: "), BorderLayout.WEST);

							JTextField filter = new JTextField();
							filter.setDocument(new Document() {

								private int filter;

								@Override
								public int getLength() {
									return filter >> 4;
								}

								@Override
								public void addDocumentListener(DocumentListener listener) {

								}

								@Override
								public void removeDocumentListener(DocumentListener listener) {

								}

								@Override
								public void addUndoableEditListener(UndoableEditListener listener) {

								}

								@Override
								public void removeUndoableEditListener(UndoableEditListener listener) {

								}

								Hashtable<Object, Object> table = new Hashtable<Object, Object>();

								@Override
								public Object getProperty(Object key) {
									return table.get(key);
								}

								@Override
								public void putProperty(Object key, Object value) {
									table.put(key, value);
								}

								@Override
								public void remove(int offs, int len) throws BadLocationException {
									if (offs + len > filter >> 6) throw new BadLocationException("Can't remove more than length", offs+len);
									filter = (((filter >> 6) - len) << 6) | (filter & (~0 << 6-2*offs) & ~(~0 << 6)) | ((filter & ~(~0 << 6-2*offs-2*len))<<2*len);
								}

								@Override
								public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {

								}

								@Override
								public String getText(int offset, int length) throws BadLocationException {
									return DNA.encode(filter & ~(~0 << 6), DNA.Mode.mRNA).substring(0, filter >> 6).substring(offset, length);
								}

								@Override
								public void getText(int offset, int length, Segment txt) throws BadLocationException {

								}

								@Override
								public Position getStartPosition() {
									return null;
								}

								@Override
								public Position getEndPosition() {
									return null;
								}

								@Override
								public Position createPosition(int offs) throws BadLocationException {
									return null;
								}

								@Override
								public Element[] getRootElements() {
									return new Element[0];
								}

								@Override
								public Element getDefaultRootElement() {
									return null;
								}

								@Override
								public void render(Runnable r) {

								}
							});
							dnaFilter.add(filter, BorderLayout.CENTER);
						}
						dnaToolbar.add(dnaFilter, BorderLayout.CENTER);
					}
					dna.add(dnaToolbar, BorderLayout.NORTH);

					JList list = new JList(new AbstractListModel() {
						@Override
						public int getSize() {
							return 64;
						}

						@Override
						public Object getElementAt(int index) {
							return DNA.encode(index, DNA.Mode.mRNA);
						}
					});
					dna.add(new JScrollPane(list, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER), BorderLayout.CENTER);
				}
				left.addTab("DNA", dna);
//		        JPanel amk = new JPanel();
//		        left.addTab("AMK", amk);
			}
			contentPane.setLeftComponent(left);

			JPanel right = new JPanel();
			{
				GridBagLayout gbl = new GridBagLayout();
				gbl.columnWidths = new int[]{0, 0};
				gbl.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
				gbl.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 2.0, Double.MIN_VALUE};
				right.setLayout(gbl);

				GridBagConstraints gbc;

				JLabel lblTitle = new JLabel("Treonín - Thr (T)");
				gbc = new GridBagConstraints();
				gbc.insets = new Insets(0, 0, 5, 0);
				gbc.gridx = 0;
				gbc.gridy = 0;
				right.add(lblTitle, gbc);

				JLabel lblSubtitle = new JLabel("en: Threonine");
				gbc = new GridBagConstraints();
				gbc.insets = new Insets(0, 0, 5, 0);
				gbc.gridx = 0;
				gbc.gridy = 1;
				right.add(lblSubtitle, gbc);

				JPanel table = new JPanel();
				{
					GridBagLayout gbl2 = new GridBagLayout();
					gbl2.columnWidths = new int[]{0, 0, 0, 0};
					gbl2.rowHeights = new int[]{14, 0};
					gbl2.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
					gbl2.rowWeights = new double[]{0.0, Double.MIN_VALUE};
					table.setLayout(gbl2);

					gbc = new GridBagConstraints();
					gbc.insets = new Insets(0, 0, 0, 5);
					gbc.anchor = GridBagConstraints.NORTH;
					gbc.gridx = 0;
					gbc.gridy = 0;
					table.add(new JLabel("mRNA"), gbc);

					gbc = new GridBagConstraints();
					gbc.insets = new Insets(0, 0, 0, 5);
					gbc.gridx = 1;
					gbc.gridy = 0;
					table.add(new JLabel("tRNA"), gbc);

					gbc = new GridBagConstraints();
					gbc.gridx = 2;
					gbc.gridy = 0;
					table.add(new JLabel("DNA"), gbc);
				}
				gbc = new GridBagConstraints();
				gbc.insets = new Insets(0, 0, 5, 0);
				gbc.fill = GridBagConstraints.BOTH;
				gbc.gridx = 0;
				gbc.gridy = 3;
				right.add(table, gbc);
			}
			contentPane.setRightComponent(right);
		}
		setContentPane(contentPane);

		setBounds(100, 100, 640, 480);
	}

	public static void main(String... args) {
		new MainFrame().setVisible(true);
	}

}
