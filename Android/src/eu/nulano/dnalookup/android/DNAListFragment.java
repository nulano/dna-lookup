/*
    DNA Lookup
    Copyright (C) 2016  Nulano (Ondrej Baranovic)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.nulano.dnalookup.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import eu.nulano.dnalookup.data.AMK;
import eu.nulano.dnalookup.data.DNA;

/**
 * Created by Nulano on 31. 5. 2016.
 *
 * @author Nulano
 */
public class DNAListFragment extends android.support.v4.app.ListFragment {

	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */
	private static final String STATE_ACTIVATED_POSITION = "activated_position";

	/** The current activated item position. Only used on tablets. */
	private int mActivatedPosition = ListView.INVALID_POSITION;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		@SuppressWarnings("SpellCheckingInspection") @SuppressLint("InlinedApi")
		final int layout = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) ?
				android.R.layout.simple_list_item_activated_2 :
				android.R.layout.simple_list_item_checked;

		final Context context = getActivity();

		setListAdapter(new BaseAdapter() {
			private final LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			@Override
			public int getCount() {
				return 1 << (6 - 2*(callbacks.getFilter() >> 6));
			}

			@Override
			public Integer getItem(int i) {
				return i + (callbacks.getFilter() & ~(~0 << 6));
			}

			@Override
			public long getItemId(int i) {
				return getItem(i);
			}

			@Override
			public boolean hasStableIds() {
				return true;
			}

			@Override
			public View getView(int i, View convertView, ViewGroup parent) {
				int item = getItem(i);
				View v = convertView == null? mInflater.inflate(layout, parent, false):convertView;
				AMK amk = DNA.AMK(item);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					TextView t = (TextView) v.findViewById(android.R.id.text1);
					t.setText(amk == AMK.STOP ? amk.skName : String.format("%s - %s (%s)", amk.abbrev,
							/*Locale.getDefault().toString().equals("sk_SK")?*/ amk.skName /*: amk.enName*/,
							/*Locale.getDefault().toString().equals("sk_SK")?*/ amk.enName /*: amk.skName*/));
					t = (TextView) v.findViewById(android.R.id.text2);
					t.setText(DNA.encode(item, callbacks.getMode()));
				} else {
					TextView t = (TextView) v.findViewById(android.R.id.text1);
					t.setText(String.format("%s - %s (%s)", amk.abbrev,
							/*Locale.getDefault().toString().equals("sk_SK")?*/ amk.skName/*:amk.enName*/,
							DNA.encode(item, callbacks.getMode())));
				}
				return v;
			}
		});
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if (savedInstanceState != null && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION))
			setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
	}

	interface Callbacks {
		void onItemSelected(int id);
		DNA.Mode getMode();
		int getFilter();
	}
	private Callbacks callbacks = null;
	private final Callbacks dummyCallbacks = new Callbacks() {
		@Override public void onItemSelected(int id) {}
		@Override public DNA.Mode getMode() { return DNA.Mode.mRNA; }
		@Override public int getFilter() { return 0; }
	};

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callbacks = (Callbacks) activity;
		} catch (ClassCastException e) {
			throw new IllegalStateException("activity must implement the required interface", e);
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		callbacks = dummyCallbacks;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position, long id) {
		callbacks.onItemSelected((int) id);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically give items the 'activated' state when touched.
		getListView().setChoiceMode(activateOnItemClick ? ListView.CHOICE_MODE_SINGLE : ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION)
			getListView().setItemChecked(mActivatedPosition, false);
		else
			getListView().setItemChecked(position, true);

		mActivatedPosition = position;
	}

}
