package eu.nulano.dnalookup.android;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by nulano on 3.6.2016.
 */
public class CodonView extends FrameLayout {

    private int code;

    private final TextView mRNA, tRNA, DNA;

    public CodonView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CodonView);
        for (int i = 0; i < a.getIndexCount(); i++) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.CodonView_code:
                    code = a.getInt(attr, 0);
                    break;
            }
        }
        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_codon, this);

        mRNA = (TextView) findViewById(R.id.mRNA);
        tRNA = (TextView) findViewById(R.id.tRNA);
         DNA = (TextView) findViewById(R.id. DNA);

        setCode(code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
        mRNA.setText(eu.nulano.dnalookup.data.DNA.mRNA(code));
        tRNA.setText(eu.nulano.dnalookup.data.DNA.tRNA(code));
         DNA.setText(eu.nulano.dnalookup.data.DNA. DNA(code));
    }
}
