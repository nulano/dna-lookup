/*
    DNA Lookup
    Copyright (C) 2016  Nulano (Ondrej Baranovic)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.nulano.dnalookup.android;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import eu.nulano.dnalookup.data.AMK;
import eu.nulano.dnalookup.data.DNA;

/**
 * Created by nulano on 3.6.2016.
 */
public class AMKFragment extends Fragment {

    private AMK amk = AMK.A;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DNA.AMK(0); // force DNA.<clinit>()
        amk = AMK.values()[getArguments().getInt("amk", 0)];
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_amk, container, false);

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(String.format("%s - %s (%s)", amk.skName, amk.abbrev, amk.name()));
        TextView subtitle = (TextView) view.findViewById(R.id.subtitle);
        subtitle.setText(String.format("en: %s", amk.enName));

        LinearLayout codonTable = (LinearLayout) view.findViewById(R.id.codonTable);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        for (Integer i : amk.codes) {
            CodonView codon = new CodonView(getActivity(), null);
            codon.setCode(i);
            codonTable.addView(codon, params);
        }

        return view;
    }

}
