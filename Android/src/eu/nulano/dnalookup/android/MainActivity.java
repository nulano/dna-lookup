/*
    DNA Lookup
    Copyright (C) 2016  Nulano (Ondrej Baranovic)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.nulano.dnalookup.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import eu.nulano.dnalookup.data.AMK;
import eu.nulano.dnalookup.data.DNA;


/**
 * Created by Nulano on 31. 5. 2016.
 *
 * @author Nulano
 */
@SuppressWarnings("FieldCanBeLocal")
public class MainActivity extends FragmentActivity implements DNAListFragment.Callbacks {

	/** Whether or not the activity is in two-pane mode, i.e. running on a tablet device. */
	private boolean mTwoPane;

	private DNA.Mode mode = DNA.Mode.mRNA;
	private static final String MODE = "dna_mode";

	private int filter; // len << 6 | filter
	private static final String FILTER = "dna_filter";

	private RadioButton rbM, rbT, rbD;
	private Button b0, b1, b2, b3;

	private EditText text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_list);

		if (findViewById(R.id.amk_detail_container) != null) {
			mTwoPane = true;
			// In two-pane mode, list items should be given the 'activated' state when touched.
			((DNAListFragment) getSupportFragmentManager().findFragmentById(R.id.dna_list)).setActivateOnItemClick(true);
		}

		CompoundButton.OnCheckedChangeListener rbListener = new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				DNA.Mode mymode;
				switch (buttonView.getId()) {
					case R.id.mRNA: mymode = DNA.Mode.mRNA; break;
					case R.id.tRNA: mymode = DNA.Mode.tRNA; break;
					case R.id. DNA: mymode = DNA.Mode. DNA; break;
					default: Log.w("main", "radio button listener called from unknown button: "+buttonView); return;
				}
				if (isChecked) {
					//Make the text underlined
					SpannableString content = new SpannableString(mymode.name());
					content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
					buttonView.setText(content);
					setMode(mymode);
				} else {
					SpannableString content = new SpannableString(mymode.name());
					content.setSpan(null, 0, content.length(), 0);
					buttonView.setText(content);
				}
			}
		};
		rbM = (RadioButton) findViewById(R.id.mRNA);
		rbT = (RadioButton) findViewById(R.id.tRNA);
		rbD = (RadioButton) findViewById(R.id. DNA);
		rbM.setOnCheckedChangeListener(rbListener);
		rbT.setOnCheckedChangeListener(rbListener);
		rbD.setOnCheckedChangeListener(rbListener);

		class ButtonListener0 implements View.OnClickListener {
			private final int id;

			public ButtonListener0(int id) {
				this.id = id;
			}

			@Override
			public void onClick(View v) {
				int i = (filter >> 6) + 1;
				if (i > 3) return;
				filter = filter & ~(~0 << 6) & ~(3 << 6-2*i) | (id << 6-2*i) | (i << 6);
				updateList();
				updateText();
			}
		}
		b0 = (Button) findViewById(R.id.b0);
		b1 = (Button) findViewById(R.id.b1);
		b2 = (Button) findViewById(R.id.b2);
		b3 = (Button) findViewById(R.id.b3);
		b0.setOnClickListener(new ButtonListener0(0));
		b1.setOnClickListener(new ButtonListener0(1));
		b2.setOnClickListener(new ButtonListener0(2));
		b3.setOnClickListener(new ButtonListener0(3));
		((Button) findViewById(R.id.bClear)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				filter = 0;
				updateText();
				updateList();
			}
		});

		text = (EditText) findViewById(R.id.dna_filter);

		if (savedInstanceState != null) {
			filter = savedInstanceState.getInt(FILTER, 0);
			setMode(DNA.Mode.values()[savedInstanceState.getInt(MODE, 0)]);
		} else setMode(DNA.Mode.mRNA);

		rbM.setChecked(mode == DNA.Mode.mRNA);
		rbT.setChecked(mode == DNA.Mode.tRNA);
		rbD.setChecked(mode == DNA.Mode. DNA);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(FILTER, filter);
		outState.putInt(MODE, mode.ordinal());
	}

	@SuppressWarnings("PointlessBitwiseExpression")
	private void setMode(DNA.Mode mode) {
		b0.setText(DNA.encode(0, mode).substring(2));
		b1.setText(DNA.encode(1, mode).substring(2));
		b2.setText(DNA.encode(2, mode).substring(2));
		b3.setText(DNA.encode(3, mode).substring(2));
		if (this.mode == mode) return;
		this.mode = mode;
		updateText();
		updateList();
	}

	private void updateText() {
		text.setText(DNA.encode(filter & ~(~0 << 6), mode).substring(0, filter >> 6));
	}

	private void updateList() {
		((BaseAdapter) ((DNAListFragment) getSupportFragmentManager().findFragmentById(R.id.dna_list))
				.getListAdapter()).notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (item.getItemId() == R.id.action_about) {
			Intent i = new Intent(this, AboutActivity.class);
			startActivity(i);
			return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public void onItemSelected(int id) {
		AMK amk = DNA.AMK(id);
		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putInt("amk", amk.ordinal());
			AMKFragment fragment = new AMKFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction().replace(R.id.amk_detail_container, fragment).commit();

		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(this, AMKDetailActivity.class);
			detailIntent.putExtra("amk", amk.ordinal());
			startActivity(detailIntent);
		}
	}

	@Override
	public DNA.Mode getMode() {
		return mode;
	}

	@Override
	public int getFilter() {
		return filter;
	}
}
