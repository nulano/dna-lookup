/*
    DNA Lookup
    Copyright (C) 2016  Nulano (Ondrej Baranovic)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.nulano.dnalookup.data;

import static eu.nulano.dnalookup.data.AMK.*;

/**
 * Created by Nulano on 31. 5. 2016.
 *
 * @author Nulano
 */
@SuppressWarnings({"unused","MethodNameSameAsClassName","WeakerAccess"})
public class DNA {
	private static final char[] DNA = {'A','G','T','C'}, mRNA = {'U','C','A','G'}, tRNA = {'A','G','U','C'};
	private static final AMK[] bind = {
			//  UU      UC      UA            UG
			F,F,L,L,S,S,S,S,Y,Y,STOP,STOP,C,C,STOP,W,
			//  CU      CC      CA      CG
			L,L,L,L,P,P,P,P,H,H,Q,Q,R,R,R,R,
			//  AU      AC      AA      AG
			I,I,I,M,T,T,T,T,N,N,K,K,S,S,R,R,
			//  GU      GC      GA      GG
			V,V,V,V,A,A,A,A,D,D,E,E,G,G,G,G
	};
	static { AMK.initCode(); }

	public static int  DNA(String  dna) { return decode( dna,  DNA); }
	public static int mRNA(String mRna) { return decode(mRna, mRNA); }
	public static int tRNA(String tRna) { return decode(tRna, tRNA); }
	public static int  DNA(char[]  dna) { return decode( dna, 0,  DNA); }
	public static int mRNA(char[] mRna) { return decode(mRna, 0, mRNA); }
	public static int tRNA(char[] tRna) { return decode(tRna, 0, tRNA); }
	public static int  DNA(char[]  dna, int off) { return decode( dna, off,  DNA); }
	public static int mRNA(char[] mRna, int off) { return decode(mRna, off, mRNA); }
	public static int tRNA(char[] tRna, int off) { return decode(tRna, off, tRNA); }
	public static String  DNA(int code) { return encode(code,  DNA); }
	public static String mRNA(int code) { return encode(code, mRNA); }
	public static String tRNA(int code) { return encode(code, tRNA); }
	public static void  DNA(int code, char[] data, int off) { encode(code,  DNA, data, off); }
	public static void mRNA(int code, char[] data, int off) { encode(code, mRNA, data, off); }
	public static void tRNA(int code, char[] data, int off) { encode(code, tRNA, data, off); }

	public static int decode(String code, Mode mode) { return decode(code, mode.chars); }
	private static int decode(String code, char[] chars) {
		if (code.length() > 3) throw new IllegalArgumentException("code too long: " + code.length());
		return decode(code.toCharArray(), 0, chars);
	}
	public static int decode(char[] code, Mode mode) { return decode(code, 0, mode.chars); }
	public static int decode(char[] code, int off, Mode mode) { return decode(code, off, mode.chars); }
	private static int decode(char[] code, int off, char[] chars) {
		if (code.length > 3) throw new IllegalArgumentException("code too long: " + code.length);
		if (chars.length != 4) throw new IllegalArgumentException("invalid char map");
		int x = 0;
		main:
		for (char c : code) {
			x <<= 2;
			for (int i = 0; i < 4; i++) {
				if (c == chars[i]) {
					x |= i;
					continue main;
				}
			}
			throw new IllegalArgumentException("unknown symbol: " + c);
		}
		return x;
	}
	public static String encode(int code, Mode mode) { return encode(code, mode.chars); }
	private static final char[] encodeData = new char[3];
	private static String encode(int code, char[] chars) {
//		synchronized (encodeData) {
		encode(code, chars, encodeData, 0);
		return new String(encodeData);
//		}
	}
	public static void encode(int code, char[] data, Mode mode) { encode(code, mode.chars, data, 0); }
	public static void encode(int code, char[] data, int off, Mode mode) { encode(code, mode.chars, data, off); }
	private static void encode(int code, char[] chars, char[] data, int off) {
		if (chars.length != 4) throw new IllegalArgumentException("invalid char map");
		if (data.length - off < 3) throw new ArrayIndexOutOfBoundsException("data index out of bounds: " + off);
		if ((code & (~0 << 6)) != 0) throw new IllegalArgumentException("invalid code");
		for (int i = 0; i < 3; i++)
			data[2 - i + off] = chars[(code >> i*2) & 3];
	}

	public static AMK AMK(int code) {
		return bind[code];
	}

	public enum Mode {
		DNA(eu.nulano.dnalookup.data.DNA.DNA),
		mRNA(eu.nulano.dnalookup.data.DNA.mRNA),
		tRNA(eu.nulano.dnalookup.data.DNA.tRNA);

		private final char[] chars;
		Mode(char[] chars) { this.chars = chars; }
	}
}