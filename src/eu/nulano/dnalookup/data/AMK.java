/*
    DNA Lookup
    Copyright (C) 2016  Nulano (Ondrej Baranovic)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.nulano.dnalookup.data;

import java.util.*;

/**
 * Created by Nulano on 31. 5. 2016.
 *
 * @author Nulano
 */
@SuppressWarnings("WeakerAccess")
public enum AMK {

	A   ("Ala",  "Alanín",          "Alanine"       ),
	R   ("Arg",  "Arginín",         "Arginine"      ),
	D   ("Asp",  "kys. Asparágová", "Aspartate"     ),
	N   ("Asn",  "Asparagín",       "Asparagine"    ),
	C   ("Cys",  "Cysteín",         "Cysteine"      ),
	E   ("Glu",  "kys. Glutámová",  "Glutamate"     ),
	Q   ("Gln",  "Glutamín",        "Glutamine"     ),
	G   ("Gly",  "Glycín",          "Glycine"       ),
	H   ("His",  "Histidín",        "Histidine"     ),
	I   ("Ile",  "Izoleucín",       "Isoleucine"    ),
	L   ("Leu",  "Leucín",          "Leucine"       ),
	K   ("Lys",  "Lyzín",           "Lysine"        ),
	M   ("Met",  "Metionín",        "Methionine"    ),
	F   ("Phe",  "Fenylalanín",     "Phenylalanine" ),
	P   ("Pro",  "Prolín",          "Proline"       ),
	S   ("Ser",  "Serín",           "Serine"        ),
	T   ("Thr",  "Treonín",         "Threonine"     ),
	W   ("Trp",  "Tryptofan",       "Tryptophan"    ),
	Y   ("Tyr",  "Tyrozín",         "Tyrosine"      ),
	V   ("Val",  "Valín",           "Valine"        ),
	STOP("STOP", "STOP",            "STOP"          );

	public final String abbrev;
	public final String skName;
	public final String enName;
	private final Set<Integer> _codes;
	public final Set<Integer> codes;

	AMK(String abbrev, String skName, String enName) {
		this.abbrev = abbrev;
		this.skName = skName;
		this.enName = enName;
		this._codes = new TreeSet<Integer>();
		this.codes = Collections.unmodifiableSet(_codes);
	}

	static void initCode() {
		for (int i = 0; i < (1 << 6); i++)
			DNA.AMK(i)._codes.add(i);
	}

}