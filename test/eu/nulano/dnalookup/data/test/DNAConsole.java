/*
    DNA Lookup
    Copyright (C) 2016  Nulano (Ondrej Baranovic)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.nulano.dnalookup.data.test;

import eu.nulano.dnalookup.data.AMK;
import eu.nulano.dnalookup.data.DNA;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Nulano on 31. 5. 2016.
 *
 * @author Nulano
 */
@SuppressWarnings("ALL")
public class DNAConsole {

	public static void main(String... args) throws IOException {
		char[] line = new char[10], chars = new char[3];
		InputStreamReader r = new InputStreamReader(System.in);
		while (true) {
			System.out.print("mRNA: ");
			int len = r.read(line);
			System.arraycopy(line, 0, chars, 0, 3);
			int code = DNA.mRNA(chars);
			AMK amk = DNA.AMK(code);
			System.out.println(code + "\t" + amk.name() + "\t" + amk.abbrev + "\t" + amk.skName + "\t" + amk.enName);
		}
	}

}