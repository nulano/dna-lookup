/*
    DNA Lookup
    Copyright (C) 2016  Nulano (Ondrej Baranovic)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.nulano.dnalookup.data.test;

import eu.nulano.dnalookup.data.AMK;
import eu.nulano.dnalookup.data.DNA;

/**
 * Created by Nulano on 31. 5. 2016.
 *
 * @author Nulano
 */
public class DNATable {

	public static void main(String... args) {
		DNA.AMK(0);
		System.out.println("\n------------------------------------------------------------\n");
		for (AMK amk : AMK.values()) {
			System.out.printf("%s - %s\t\tSK: %s\t\tEN: %s\n", amk.name(), amk.abbrev, amk.skName, amk.enName);
			System.out.println("\n DNA\tmRNA\ttRNA\n");
			for (Integer i : amk.codes) {
				System.out.printf(" %s\t %s\t %s\n", DNA.DNA(i), DNA.mRNA(i), DNA.tRNA(i));
			}
			System.out.println("\n------------------------------------------------------------\n");
		}
	}

}